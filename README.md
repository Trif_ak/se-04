## SE-04

    SE-04 is a program project manager

## TECHNOLOGY STACK

    Maven 4.0
    Java SE 1.8
    Junit 4.11

## DEVELOPER

    Trifonov Ilya
    trifakk@gmail.com

## SOFTWARE REQUIREMENTS
    jdk 1.6

## USING THE PROJECT MANAGER

    From the command-line

    Download the project manager and run it with:

    java -jar D:\WorkSpaceIlyaTrifonov\SE-04\target\se-04-1.0-SNAPSHOT.jar

## BUILDING FROM SOURCE

    mvn install