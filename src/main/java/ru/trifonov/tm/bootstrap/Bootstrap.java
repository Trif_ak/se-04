package ru.trifonov.tm.bootstrap;

import ru.trifonov.tm.api.ProjectRepository;
import ru.trifonov.tm.api.TaskRepository;
import ru.trifonov.tm.repository.*;
import ru.trifonov.tm.service.*;
import ru.trifonov.tm.view.*;
import java.util.Scanner;

public class Bootstrap {
    private String command;
    private Scanner inCommand = new Scanner(System.in);

    private ProjectRepository projectRepository = new IProjectRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskRepository taskRepository = new ITaskRepository();
    private TaskService taskService = new TaskService(taskRepository);

    /*Constructor*/

    private MenuOfProjectManager menuOfProjectManager = new MenuOfProjectManager(
            inCommand,
            taskService,
            projectService);

    /*View method*/

    public void init() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("Enter \"help\" for show all commands. ");

        do {
            command = inCommand.nextLine();
            switch (command) {
                case (CommandForProjectManager.H):
                case (CommandForProjectManager.HELP):
                    menuOfProjectManager.help();
                    break;

                /*Projects commands*/

                case (CommandForProjectManager.PP):
                case (CommandForProjectManager.PROJECT_PERSIST):
                    menuOfProjectManager.persistProject();
                    break;

                case (CommandForProjectManager.PM):
                case (CommandForProjectManager.PROJECT_MERGE):
                    menuOfProjectManager.mergeProject();
                    break;

                case (CommandForProjectManager.PF):
                case (CommandForProjectManager.PROJECT_FIND_ONE):
                    menuOfProjectManager.findOneProject();
                    break;
                case (CommandForProjectManager.PFA):
                case (CommandForProjectManager.PROJECT_FIND_ALL):
                    menuOfProjectManager.findAllProject();
                    break;

                case (CommandForProjectManager.PR):
                case (CommandForProjectManager.PROJECT_REMOVE):
                    menuOfProjectManager.removeProject();
                    break;

                case (CommandForProjectManager.PRA):
                case (CommandForProjectManager.PROJECT_REMOVE_ALL):
                    menuOfProjectManager.removeAllProject();
                    break;

                /*TaskRepository commands*/

                case (CommandForProjectManager.TP):
                case (CommandForProjectManager.TASK_PERSIST):
                    menuOfProjectManager.persistTask();
                    break;

                case (CommandForProjectManager.TM):
                case (CommandForProjectManager.TASK_MERGE):
                    menuOfProjectManager.mergeTask();
                    break;

                case (CommandForProjectManager.TF):
                case (CommandForProjectManager.TASK_FIND_ONE):
                    menuOfProjectManager.findOneTask();
                    break;
                case (CommandForProjectManager.TFA):
                case (CommandForProjectManager.TASK_FIND_ALL):
                    menuOfProjectManager.findAllTask();
                    break;

                case (CommandForProjectManager.TR):
                case (CommandForProjectManager.TASK_REMOVE):
                    menuOfProjectManager.removeTask();
                    break;
                case (CommandForProjectManager.TRA):
                case (CommandForProjectManager.TASK_REMOVE_ALL):
                    menuOfProjectManager.removeAllTask();
                    break;
            }
        } while (!command.equals("exit"));
    }
}
