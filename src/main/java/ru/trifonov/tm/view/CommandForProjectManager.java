package ru.trifonov.tm.view;

public class CommandForProjectManager {

    public static final String HELP = "help";
    public static final String H = "h";


//    Project command

    public static final String PROJECT_PERSIST = "project-persist";
    public static final String PP = "pp";
    public static final String PROJECT_MERGE = "project-merge";
    public static final String PM = "pm";
    public static final String PROJECT_FIND_ONE = "project-findOne";
    public static final String PF = "pf";
    public static final String PROJECT_FIND_ALL = "project-findAll";
    public static final String PFA = "pfa";
    public static final String PROJECT_REMOVE = "project-remove";
    public static final String PR = "pr";
    public static final String PROJECT_REMOVE_ALL = "project-removeAll";
    public static final String PRA = "pra";

//    TaskRepository command

    public static final String TASK_PERSIST = "task-persist";
    public static final String TP = "tp";
    public static final String TASK_MERGE = "task-merge";
    public static final String TM = "tm";
    public static final String TASK_FIND_ONE = "task-findOne";
    public static final String TF = "tf";
    public static final String TASK_FIND_ALL = "task-findAll";
    public static final String TFA = "tfa";
    public static final String TASK_REMOVE = "task-remove";
    public static final String TR = "tr";
    public static final String TASK_REMOVE_ALL = "task-removeAll";
    public static final String TRA = "tra";
}
