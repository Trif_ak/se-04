package ru.trifonov.tm.view;

import ru.trifonov.tm.service.ProjectService;
import ru.trifonov.tm.service.TaskService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

public class MenuOfProjectManager {
    private Scanner scanner;
    private TaskService taskService;
    private ProjectService projectService;

    private String name;
    private String idProject;
    private String idTask;
    private String description;
    private String startDate;
    private String finishDate;
    private List<String> inputList = new ArrayList<>();

    /*Constructor*/

    public MenuOfProjectManager(
            Scanner scanner,
            TaskService taskService,
            ProjectService projectService)
    {
        this.scanner = scanner;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    /*Implementation command*/

    public void help() {
        System.out.println(" help: Show all commands. " +
                "\n project-insert (pc): Create new project." +
                "\n project-update (pu): Update selected project." +
                "\n project-list (pl): Show all projects." +
                "\n project-remove (pr): Remove selected project." +
                "\n project-removeAll (pcl): Remove all projects." +
                "\n" +
                "\n task-insert (tc): Create new task." +
                "\n task-update (tu): Update selected task." +
                "\n task-list (tl): Show all tasks." +
                "\n task-remove (tr): Remove selected task." +
                "\n task-removeAll (tcl): Remove all tasks." +
                "\n " +
                "\n exit: Exit from app.");
    }

    /*Command for project*/

    public void persistProject() {
        System.out.println("[PROJECT PERSIST]");
        System.out.println("Enter name");
        name = scanner.nextLine();
        System.out.println("Enter description");
        description = scanner.nextLine();
        System.out.println("Enter start date. Date format DD.MM.YYYY");
        startDate = scanner.nextLine();
        System.out.println("Enter finish date. Date format DD.MM.YYYY");
        finishDate = scanner.nextLine();
        projectService.persist(name, UUID.randomUUID().toString(), description, startDate, finishDate);
        System.out.println("[OK]");
    }

    public void mergeProject() {
        System.out.println("PROJECT MERGE");
        System.out.println("Enter the ID of the project you want to update");
        idProject = scanner.nextLine();
        System.out.println("Enter new name");
        name = scanner.nextLine();
        System.out.println("Enter new description");
        description = scanner.nextLine();
        System.out.println("Enter new start date. Date format DD.MM.YYYY");
        startDate = scanner.nextLine();
        System.out.println("Enter new finish date. Date format DD.MM.YYYY");
        finishDate = scanner.nextLine();
        projectService.merge(name, idProject, description, startDate, finishDate);
        System.out.println("[OK]");
    }

    public void updateProject() {
        System.out.println("PROJECT UPDATE");
        System.out.println("Enter the ID of the project you want to update");
        idProject = scanner.nextLine();
        System.out.println("Enter new name");
        name = scanner.nextLine();
        System.out.println("Enter new description");
        description = scanner.nextLine();
        System.out.println("Enter new start date. Date format DD.MM.YYYY");
        startDate = scanner.nextLine();
        System.out.println("Enter new finish date. Date format DD.MM.YYYY");
        finishDate = scanner.nextLine();
        projectService.merge(name, idProject, description, startDate, finishDate);
        System.out.println("[OK]");
    }

    public void findOneProject() {
        System.out.println("[FIND PROJECT]");
        System.out.println("Enter the ID of the project");
        idProject = scanner.nextLine();
        System.out.println(projectService.findOne(idProject));
        System.out.println("[OK]");
    }

    public void findAllProject() {
        System.out.println("[FIND ALL PROJECT]");
        inputList = projectService.findAll();
        for (String string : inputList) {
            System.out.println(string);
        }
        System.out.println("[OK]");
    }

    public void removeProject() {
        System.out.println("[REMOVE PROJECT]");
        findAllProject();
        System.out.println("Enter the ID of the project you want to remove");
        idProject = scanner.nextLine();
        taskService.removeAll(idProject);
        projectService.remove(idProject);
        System.out.println("[OK]");
    }

    public void removeAllProject() {
        System.out.println("[REMOVE ALL PROJECT]");
        taskService.removeRealAll();
        projectService.removeAll();
        System.out.println("[OK]");
    }

    /*Command for task*/

    public void persistTask() {
        System.out.println("[PERSIST TASK]");
        findAllProject();
        System.out.println("Enter ID of project");
        idProject = scanner.nextLine();
        System.out.println("Enter name task");
        name = scanner.nextLine();
        System.out.println("Enter description");
        description = scanner.nextLine();
        System.out.println("Enter start date task. Date format DD.MM.YYYY");
        startDate = scanner.nextLine();
        System.out.println("Enter finish date task. Date format DD.MM.YYYY");
        finishDate = scanner.nextLine();
        taskService.persist(name, UUID.randomUUID().toString(), idProject, description, startDate, finishDate);
        System.out.println("[OK]");
    }

    public void mergeTask() {
        System.out.println("[MERGE TASK]");
        findAllTask();
        System.out.println("Enter the ID of the task you want to update");
        idTask = scanner.nextLine();
        System.out.println("Enter new name");
        name = scanner.nextLine();
        System.out.println("Enter new idProject");
        idProject = scanner.nextLine();
        System.out.println("Enter new description");
        description = scanner.nextLine();
        System.out.println("Enter new start date task. Date format DD.MM.YYYY");
        startDate = scanner.nextLine();
        System.out.println("Enter new finish date task. Date format DD.MM.YYYY");
        name = scanner.nextLine();
        taskService.merge(name, idTask, idProject, description, startDate, finishDate);
        System.out.println("[OK]");
    }

    public void findOneTask() {
        System.out.println("[FIND TASK]");
        findAllTask();
        System.out.println("Enter the ID of the task");
        idTask = scanner.nextLine();
        System.out.println(taskService.findOne(idTask));
        System.out.println("[OK]");
    }

    public void findAllTask() {
        System.out.println("[FIND ALL TASK]");
        findAllProject();
        System.out.println("Enter ID of project");
        idProject = scanner.nextLine();
        inputList = taskService.findAll(idProject);
        for (String string : inputList) {
            System.out.println(string);
        }
        System.out.println("[OK]");
    }

    public void removeTask() {
        System.out.println("[REMOVE TASK]");
        findAllTask();
        System.out.println("Enter the ID of the task you want to remove");
        idTask = scanner.nextLine();
        taskService.remove(idTask);
        System.out.println("[OK]");
    }

    public void removeAllTask() {
        System.out.println("[REMOVE ALL TASK]");
        findAllProject();
        System.out.println("Enter ID of project");
        idProject = scanner.nextLine();
        taskService.removeAll(idProject);
        System.out.println("[OK]");
    }
}
