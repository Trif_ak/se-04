package ru.trifonov.tm.repository;

import ru.trifonov.tm.api.ProjectRepository;
import ru.trifonov.tm.entity.*;

import java.util.*;

public class IProjectRepository implements ProjectRepository {
    private String output;
    private List<String> outputList = new ArrayList<>();
    private Map<String, ru.trifonov.tm.entity.Project> projects = new HashMap<>();

    /*CRUD methods*/
    @Override
    public void persist(Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public void merge(Project project) {
        if (projects.containsKey(project.getId())) {
            update(project.getName(), project.getId(), project.getDescription(), project.getBeginDate(), project.getEndDate());
        } else
            insert(project.getName(), project.getId(), project.getDescription(), project.getBeginDate(), project.getEndDate());
    }

    @Override
    public void insert(String name, String id, String description, Date beginDate, Date endDate) {
        Project project = new Project();
        project.setName(name);
        project.setId(id);
        project.setDescription(description);
        project.setBeginDate(beginDate);
        project.setEndDate(endDate);
        projects.put(id, project);
    }

    @Override
    public void update(String name, String id, String description, Date beginDate, Date endDate) {
        Project project = new Project();
        project.setName(name);
        project.setId(id);
        project.setDescription(description);
        project.setBeginDate(beginDate);
        project.setEndDate(endDate);
        projects.put(id, project);
    }

    @Override
    public String findOne(String id) {
        for (Map.Entry<String, ru.trifonov.tm.entity.Project> project : projects.entrySet()) {
            if (project.getValue().getId().equals(id)) {
                output =
                        " ID  " + project.getValue().getId() +
                        "\n NAME  " + project.getValue().getName() +
                        "\n DESCRIPTION  " + project.getValue().getDescription() +
                        "\n PROJECT START DATE  " + project.getValue().getBeginDate() +
                        "\n PROJECT FINISH DATE  " + project.getValue().getEndDate();
                break;
            }
        }
        return output;
    }

    @Override
    public List<String> findAll() {
        outputList.clear();
        for (Map.Entry<String, Project> project : projects.entrySet()) {
            outputList.add("ID " + project.getValue().getId() + "  NAME " + project.getValue().getName());
        }
        return outputList;
    }

    @Override
    public void remove(String id) {
        Iterator<Map.Entry<String, Project>> entryIterator = projects.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, ru.trifonov.tm.entity.Project> projectEntry = entryIterator.next();
            if (projectEntry.getValue().getId().equals(id)) {
                entryIterator.remove();
                break;
            }
        }
    }

    @Override
    public void removeAll() {
        projects.clear();
    }
}
