package ru.trifonov.tm.repository;

import ru.trifonov.tm.api.TaskRepository;
import ru.trifonov.tm.entity.Task;

import java.util.*;

public class ITaskRepository implements TaskRepository {
    private String output;
    private List<String> outputList = new ArrayList<>();
    private Map<String, Task> tasks = new HashMap<>();

    @Override
    public void merge(Task task) {
        if (tasks.containsKey(task.getId())) {
            update(task.getName(), task.getId(), task.getIdProject(), task.getDescription(), task.getStartDate(), task.getFinishDate());
        } else
            insert(task.getName(), task.getId(), task.getIdProject(), task.getDescription(), task.getStartDate(), task.getFinishDate());
    }

    @Override
    public void persist(Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public void insert(String name, String id, String idProject, String description, String startDate, String finishDate) {
        Task task = new Task();
        task.setName(name);
        task.setId(id);
        task.setIdProject(idProject);
        task.setDescription(description);
        task.setStartDate(startDate);
        task.setFinishDate(finishDate);
        tasks.put(id, task);
    }

    @Override
    public void update(String name, String id, String idProject, String description, String startDate, String finishDate) {
        Task task = new Task();
        task.setName(name);
        task.setId(id);
        task.setIdProject(idProject);
        task.setDescription(description);
        task.setStartDate(startDate);
        task.setFinishDate(finishDate);
        tasks.put(id, task);
    }

    @Override
    public String findOne(String id) {
        for (Map.Entry<String, Task> task : tasks.entrySet()) {
            if (task.getValue().getId().equals(id)) {
                output =
                        " ID  " + task.getValue().getId() +
                        "\n NAME  " + task.getValue().getName() +
                        "\n DESCRIPTION  " + task.getValue().getDescription() +
                        "\n PROJECT START DATE  " + task.getValue().getStartDate() +
                        "\n PROJECT FINISH DATE  " + task.getValue().getFinishDate();
                break;
            }
        }
        return output;
    }

    @Override
    public List findAll(String idProject) {
        outputList.clear();
        for (Map.Entry<String, Task> task : tasks.entrySet()) {
            if (task.getValue().getIdProject().equals(idProject)) {
                outputList.add("ID " + task.getValue().getId() + " NAME " + task.getValue().getName());
            }
        }
        return outputList;
    }

    @Override
    public void remove(String id) {
        Iterator<Map.Entry<String, Task>> entryIterator = tasks.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> projectEntry = entryIterator.next();
            if (projectEntry.getValue().getId().equals(id)) {
                entryIterator.remove();
                break;
            }
        }
    }

    @Override
    public void removeAll(String idProject) {
        Iterator<Map.Entry<String, Task>> entryIterator = tasks.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> projectEntry = entryIterator.next();
            if (projectEntry.getValue().getIdProject().equals(idProject)) {
                entryIterator.remove();
            }
        }
    }

    @Override
    public void removeRealAll() {
        tasks.clear();
    }
}
