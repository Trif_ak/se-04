package ru.trifonov.tm;

import ru.trifonov.tm.bootstrap.Bootstrap;

public class Application {
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}