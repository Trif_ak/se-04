package ru.trifonov.tm.api;

import ru.trifonov.tm.entity.*;

import java.util.Date;
import java.util.List;

public interface ProjectRepository {
    void insert(String name, String id, String description, Date beginDate, Date endDate);
    void update(String name, String id, String description, Date beginDate, Date endDate);
    String findOne(String id);
    List findAll();
    void remove(String id);
    void removeAll();
    void merge(Project project);
    void persist(Project project);
}
