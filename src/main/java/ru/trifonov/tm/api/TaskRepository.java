package ru.trifonov.tm.api;

import ru.trifonov.tm.entity.Task;

import java.util.List;

public interface TaskRepository {
    void insert(String name, String id, String idProject, String description, String startDate, String finishDate);
    void update(String name, String id, String idProject, String description, String startDate, String finishDate);
    List<String> findAll(String idProject);
    String findOne(String id);
    void remove(String id);
    void removeAll(String idProject);
    void persist(Task task);
    void merge(Task task);
    void removeRealAll();
}
