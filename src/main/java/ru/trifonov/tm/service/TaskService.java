package ru.trifonov.tm.service;

import ru.trifonov.tm.api.TaskRepository;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.repository.ITaskRepository;

import java.util.ArrayList;
import java.util.List;

public class TaskService {
    private String output;
    private List<String> outputList = new ArrayList<>();
    private TaskRepository taskRepository;

    /*Constructor*/

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /*Check CRUD methods*/

    public void persist(String name, String id, String idProject, String description, String startDate, String finishDate) {
        if (name == null && !name.isEmpty()) return;
        if (id == null && !id.isEmpty()) return;
        if (idProject == null && !idProject.isEmpty()) return;
        if (description == null && !description.isEmpty()) return;
        if (startDate == null && !startDate.isEmpty()) return;
        if (finishDate == null && !finishDate.isEmpty()) return;
        Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setStartDate(startDate);
        task.setFinishDate(finishDate);
        taskRepository.persist(task);
    }

    public void merge(String name, String id, String idProject, String description, String startDate, String finishDate) {
        if (name == null && !name.isEmpty()) return;
        if (id == null && !id.isEmpty()) return;
        if (idProject == null && !idProject.isEmpty()) return;
        if (description == null && !description.isEmpty()) return;
        if (startDate == null && !startDate.isEmpty()) return;
        if (finishDate == null && !finishDate.isEmpty()) return;
        Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setStartDate(startDate);
        task.setFinishDate(finishDate);
        taskRepository.persist(task);
    }

    public void insert(String name, String id, String idProject, String description, String startDate, String finishDate) {
        if (name == null && !name.isEmpty()) return;
        if (id == null && !id.isEmpty()) return;
        if (idProject == null && !idProject.isEmpty()) return;
        if (description == null && !description.isEmpty()) return;
        if (startDate == null && !startDate.isEmpty()) return;
        if (finishDate == null && !finishDate.isEmpty()) return;
        taskRepository.insert(name, id, idProject, description, startDate, finishDate);
    }

    public void update(String name, String id, String idProject, String description, String startDate, String finishDate) {
        if (name == null && !name.isEmpty()) return;
        if (id == null && !id.isEmpty()) return;
        if (idProject == null && !idProject.isEmpty()) return;
        if (description == null && !description.isEmpty()) return;
        if (startDate == null && !startDate.isEmpty()) return;
        if (finishDate == null && !finishDate.isEmpty()) return;
        taskRepository.update(name, id, idProject, description, startDate, finishDate);
    }

    public String findOne(String id) {
        if (id == null && !id.isEmpty()) return null;
        output = taskRepository.findOne(id);
        return output;
    }

    public List<String> findAll(String idProject) {
        if (idProject == null && !idProject.isEmpty()) return null;
        outputList = taskRepository.findAll(idProject);
        return outputList;
    }

    public void remove(String id) {
        if (id == null && !id.isEmpty()) return;
        taskRepository.remove(id);
    }

    public void removeAll(String idProject) {
        if (idProject == null && !idProject.isEmpty()) return;
        taskRepository.removeAll(idProject);
    }

    public void removeRealAll() {
        taskRepository.removeRealAll();
    }
}
