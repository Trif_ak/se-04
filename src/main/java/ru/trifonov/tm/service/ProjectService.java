package ru.trifonov.tm.service;

import ru.trifonov.tm.api.ProjectRepository;
import ru.trifonov.tm.entity.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ProjectService {
    private Date beginDate;
    private Date endDate;
    private String output;
    private ProjectRepository projectRepository;
    SimpleDateFormat dateFormat = new SimpleDateFormat("DD.MM.YYYY");

    /*Constructor*/

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /*Check CRUD methods*/

    public void persist(String name, String id, String description, String startDate, String finishDate) {
        if (name == null && name.isEmpty()) return;
        if (id == null && id.isEmpty()) return;
        if (description == null && description.isEmpty()) return;
        if (startDate == null && startDate.isEmpty()) return;
        if (finishDate == null && finishDate.isEmpty()) return;
        try {
            beginDate = dateFormat.parse(startDate);
            endDate = dateFormat.parse(finishDate);
        } catch (Exception e) {
            e.toString();
        }
        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setBeginDate(beginDate);
        project.setEndDate(endDate);
        projectRepository.persist(project);
    }

    public void merge(String name, String id, String description, String startDate, String finishDate) {
        if (name == null && name.isEmpty()) return;
        if (id == null && id.isEmpty()) return;
        if (description == null && description.isEmpty()) return;
        if (startDate == null && startDate.isEmpty()) return;
        if (finishDate == null && finishDate.isEmpty()) return;
        try {
            beginDate = dateFormat.parse(startDate);
            endDate = dateFormat.parse(finishDate);
        } catch (Exception e) {
            e.toString();
        }
        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setBeginDate(beginDate);
        project.setEndDate(endDate);
        projectRepository.merge(project);
    }

    public void insert(String name, String id, String description, String startDate, String finishDate)
    {
        if (name == null && name.isEmpty()) return;
        if (id == null && id.isEmpty()) return;
        if (description == null && description.isEmpty()) return;
        if (startDate == null && startDate.isEmpty()) return;
        if (finishDate == null && finishDate.isEmpty()) return;
        try {
            beginDate = dateFormat.parse(startDate);
            endDate = dateFormat.parse(finishDate);
        } catch (Exception e) {
            e.toString();
        }
        projectRepository.insert(name, id, description, endDate, endDate);
    }

    public void update(String name, String id, String description, String startDate, String finishDate) {
        if (name == null && name.isEmpty()) return;
        if (id == null && id.isEmpty()) return;
        if (description == null && description.isEmpty()) return;
        if (startDate == null && startDate.isEmpty()) return;
        if (finishDate == null && finishDate.isEmpty()) return;
        try {
            beginDate = dateFormat.parse(startDate);
            endDate = dateFormat.parse(finishDate);
        } catch (Exception e) {
            e.toString();
        }
        projectRepository.update(name, id, description, beginDate, endDate);
    }
    public String findOne(String id) {
        if (id == null && id.isEmpty()) return null;
        output = projectRepository.findOne(id);
        return output;
    }
    public List<String> findAll() {
        return projectRepository.findAll();
    }

    public void remove(String id) {
        if (id == null && id.isEmpty()) return;
        projectRepository.remove(id);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }
}
